const tipButtons = document.querySelectorAll('.tip button');
const bill = document.querySelector('#Bill');
const custom = document.querySelector('#custom');
const numOfPeople = document.querySelector('#NumberOfPeople');
const tipAmount = document.querySelector('#tPP');
const totalAmount = document.querySelector('#tp');
const resetButton = document.querySelector('#reset');
const parentTip = document.querySelector('.tip');
// const submitButton = document.querySelector('.btn-dark');
let percentage = 0;

// listening to the input box Bill
bill.addEventListener('input', () => {
  if (numOfPeople.value) {
    calculateTip();
  }
});

// tipButtons.forEach((button) => {
//   button.addEventListener('click', getPercentage);
// });
//

// using event delegation
parentTip.addEventListener('click', (e) => {
  if (e.target.nodeName === 'BUTTON') {
    getPercentage(e);
  }
});

// when the focus switches to custom box, change back the colors of tip boxes
custom.addEventListener('focus', revertButtonColors);

// calling the function to getCustomValue from custom input box
custom.addEventListener('input', getCustomValue);

// event listener on input at numOfPeople input box,
// if there is an input or value - then call the function to calculateTip();
numOfPeople.addEventListener('input', (e) => {
  if (e.target.value) {
    calculateTip(); // if the value is present then call the calculateTip function
  } else {
    tipAmount.textContent = '$0.00';
    totalAmount.textContent = '$0.00';
  }
});
resetButton.addEventListener('click', reset);

// changing back the colors
// calling this function when custom value box is selected
function revertButtonColors() {
  tipButtons.forEach((button) => {
    button.style.backgroundColor = 'rgb(0, 73, 77)';
  });
  percentage = '';
}

function getCustomValue(e) {
  percentage = Number(e.target.value);
  calculateTip();
}

function getPercentage(e) {
  percentage = Number(e.target.value);

  // first check if the input tip button is already selected,
  // if selected then unselect it, and change back the colors
  // also clear out the percentage value
  tipButtons.forEach((button) => {
    if (e.target != button) button.style.backgroundColor = 'rgb(0, 73, 77)';
  });
  if (e.target.style.backgroundColor === 'red') {
    e.target.style.backgroundColor = 'rgb(0, 73, 77)';
    percentage = ''; // clearing out percentage value if clicked twice
  } else {
    e.target.style.backgroundColor = 'red';
  }
  // clearing custom value
  custom.value = '';
  calculateTip();
}

function calculateTip() {
  let ppl = Number(numOfPeople.value); //getting the value from tip Box selected
  let amt = Number(bill.value); // getting the value from bill input box
  if (amt >= 0 && ppl >= 1) {
    let totalTip = (percentage * amt) / 100;
    let totalAmt = amt + totalTip;
    let perPersonTip = (totalTip / ppl).toFixed(2);
    let totalAmtPerPerson = (totalAmt / ppl).toFixed(2);
    tipAmount.textContent = '$' + perPersonTip; // assigning it to the tipAmount html
    totalAmount.textContent = '$' + totalAmtPerPerson;
  }
}

// resetting all values when the reset button is clicked
function reset() {
  bill.value = undefined;
  revertButtonColors();
  custom.value = undefined;
  numOfPeople.value = undefined;
  tipAmount.textContent = '$0.00';
  totalAmount.textContent = '$0.00';
}
